import React, { useState } from "react";
import "../css/login/style.css";
import { useDispatch } from "react-redux";
import { handleLogin } from "../app/reducers/auth/authSlice";
import { Link, useHistory } from "react-router-dom";

const Login = (props) => {
  // State
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  // Event
  const changeEmail = (event) => {
    setEmail(event.target.value);
  };
  const history = useHistory();
  const dispatch = useDispatch();

  const userHandleLogin = () => {
    const formData = {
      email,
      password,
    };
    dispatch(handleLogin(formData))
      .unwrap()
      .then((originalPromiseResult) => {
        history.push("/");
      })
      .catch((rejectedValueOrSerializedError) => {
        alert("Email or password is not correct, please try again!");
      });
  };

  const changePassword = (event) => {
    setPassword(event.target.value);
  };

  // Template
  return (
    <section className="login">
      <div className="content">
        <div className="title">Login</div>

        <form>
          <div className="form-input">
            <input
              type="text"
              name=""
              id=""
              placeholder="Enter your email"
              autoComplete="true"
              onChange={changeEmail}
              value={email}
            />

            <input
              type="password"
              placeholder="Enter your password"
              autoComplete="true"
              onChange={changePassword}
              value={password}
            />

            <input
              type="button"
              onClick={userHandleLogin}
              className="btn-form"
              value="Login"
            />
          </div>
          <Link to={"/"}>Home Page</Link>
        </form>
      </div>
    </section>
  );
};

export default Login;
