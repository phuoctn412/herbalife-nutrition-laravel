import React from "react";
import style from "../css/common/main.module.css";
import MainHeader from "../components/MainHeader";
const About = () => {
  return (
    <section>
      <MainHeader />
      <div className={style.container}>
        <p>About</p>
      </div>
    </section>
  );
};

export default About;
