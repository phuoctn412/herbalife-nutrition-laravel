import React from "react";
import HeathyBlog from "../components/blog/HeathyBlog";
import MainHeader from "../components/MainHeader";
import style from "../css/common/main.module.css";
import "../css/heathy-blog/main.css";

const Heathy = () => {
  return (
    <section>
      <MainHeader />
      <div className={style.container}>
        <h1>Chuyên mục sức khỏe</h1>
        <div className="blog-category">
          <HeathyBlog className="blog-category-child" />
          <HeathyBlog className="blog-category-child" />
          <HeathyBlog className="blog-category-child" />
          <HeathyBlog className="blog-category-child" />
          <HeathyBlog className="blog-category-child" />
          <HeathyBlog className="blog-category-child" />
          <HeathyBlog className="blog-category-child" />
          <HeathyBlog className="blog-category-child" />
        </div>
      </div>
    </section>
  );
};

export default Heathy;
