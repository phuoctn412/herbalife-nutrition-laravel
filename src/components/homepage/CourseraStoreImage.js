import React from "react";
import img1 from "../../images/anhdep.jpeg";
import "./style.css";

const CourseraStoreImage = () => {
  return (
    <section>
      <div className="figure">
        <img src={img1} alt="" />
      </div>
    </section>
  );
};

export default CourseraStoreImage;
