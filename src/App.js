import React from "react";
import Home from "./views/Home";

import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Healthy from "./views/Heathy";
import About from "./views/About";
import Login from "./views/Login";

function App() {
  return (
    <div className="App">
      <Router>
        <Switch>
          <Route exact path="/">
            <Home />
          </Route>
          <Route path="/healthy">
            <Healthy />
          </Route>
          <Route path="/about">
            <About />
          </Route>
          <Route exact path="/login">
            <Login />
          </Route>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
